#! /bin/bash
# Miguel Amorós - Ansible
# instal.lacio
# -------------------------------------

# Creació d'usuaris locals ------------------------------------------
for user in local1 local2 local3
do
	useradd $user
	echo "$user" | passwd --stdin $user
done
