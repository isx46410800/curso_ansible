#! /bin/bash
# Miguel Amorós - Ansible
# startup.sh
# -------------------------------------
# Hacer fichero de instalación
/opt/docker/install.sh && echo "Install Ok"

# Deixar-lo en foreground
/sbin/sshd
/sbin/httpd -D FOREGROUND
