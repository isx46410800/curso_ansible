# Version: 0.0.1
# Miguel Amorós
# ssh
# -------------------------------------
FROM fedora:27
LABEL author="Miguel Amorós"
LABEL description="Fedora 27 con ssh server y clients"
RUN dnf -y install httpd procps passwd iproute nmap vim tree openssh-clients openssh-server net-tools
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/install.sh /opt/docker/startup.sh
WORKDIR /opt/docker
CMD ["/opt/docker/startup.sh"]
